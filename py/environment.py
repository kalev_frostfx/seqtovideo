# Global paths & values

# referencing paths for executables and other essential things.
# this will make it easier to add linux support down the line

# paths are relative to working dir which is the folder "py"

# executable paths WIN64
BIN_PYTHON = "..\\WPy64-3850\\scripts\\python.bat" # bundled portable python
BIN_OIIOTOOL = "..\\bin\\OpenImageIO-1.6.18-bin-vc9-x64\\oiiotool.exe"
BIN_OCIO_IINFO = "..\\bin\\OpenImageIO-1.6.18-bin-vc9-x64\\iinfo.exe"
BIN_FFMPEG = "..\\bin\\ffmpeg.exe"

# other paths
OCIO_CONFIG = "../OcioConfig/aces_1.0.3/config.ocio"
FONT = "../resources/Inconsolata-Regular.ttf"
FROSTFX_LOGO = "../resources/frostfx_logo.png"
FROSTFX_ICON = "../resources/frostfx_logo.ico"