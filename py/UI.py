#!/usr/bin/python
# -*- coding: utf-8 -*-

import tkinter as tk
from tkinter import filedialog
#import tkFileDialog
#import tkMessageBox
from tkinter import ttk
from PIL import ImageTk, Image
#import ttk
import os

from environment import FROSTFX_LOGO, FROSTFX_ICON


# tk UI
root = tk.Tk()
root.iconbitmap( default=FROSTFX_ICON )
root.geometry("600x600")
root.title("Seq to Video")


VIDEO_FORMATS = ('mp4', 'mov')

# need to read these from ocio config file in the future to make updating easier
COLOR_SPACES = (
    "ACES - ACES2065-1",
    "ACES - ACEScc",
    "ACES - ACEScct",
    "ACES - ACESproxy",
    "ACES - ACEScg",
    "Input - ADX - ADX10",
    "Input - ADX - ADX16",
    "Input - ARRI - V3 LogC (EI160) - Wide Gamut",
    "Input - ARRI - V3 LogC (EI200) - Wide Gamut",
    "Input - ARRI - V3 LogC (EI250) - Wide Gamut",
    "Input - ARRI - V3 LogC (EI320) - Wide Gamut",
    "Input - ARRI - V3 LogC (EI400) - Wide Gamut",
    "Input - ARRI - V3 LogC (EI500) - Wide Gamut",
    "Input - ARRI - V3 LogC (EI640) - Wide Gamut",
    "Input - ARRI - V3 LogC (EI800) - Wide Gamut",
    "Input - ARRI - V3 LogC (EI1000) - Wide Gamut",
    "Input - ARRI - V3 LogC (EI1280) - Wide Gamut",
    "Input - ARRI - V3 LogC (EI1600) - Wide Gamut",
    "Input - ARRI - V3 LogC (EI2000) - Wide Gamut",
    "Input - ARRI - V3 LogC (EI2560) - Wide Gamut",
    "Input - ARRI - V3 LogC (EI3200) - Wide Gamut",
    "Input - ARRI - Curve - V3 LogC (EI800)",
    "Input - ARRI - Linear - ALEXA Wide Gamut",
    "Input - Canon - Canon-Log - Rec. 709 Daylight",
    "Input - Canon - Canon-Log - Rec. 709 Tungsten",
    "Input - Canon - Canon-Log - DCI-P3 Daylight",
    "Input - Canon - Canon-Log - DCI-P3 Tungsten",
    "Input - Canon - Canon-Log - Cinema Gamut Daylight",
    "Input - Canon - Canon-Log - Cinema Gamut Tungsten",
    "Input - Canon - Canon-Log - Rec. 2020 Daylight",
    "Input - Canon - Canon-Log - Rec. 2020 Tungsten",
    "Input - Canon - Canon-Log2 - Rec. 2020 Daylight",
    "Input - Canon - Canon-Log2 - Rec. 2020 Tungsten",
    "Input - Canon - Canon-Log2 - Cinema Gamut Daylight",
    "Input - Canon - Canon-Log2 - Cinema Gamut Tungsten",
    "Input - Canon - Canon-Log3 - Rec. 2020 Daylight",
    "Input - Canon - Canon-Log3 - Rec. 2020 Tungsten",
    "Input - Canon - Canon-Log3 - Cinema Gamut Daylight",
    "Input - Canon - Canon-Log3 - Cinema Gamut Tungsten",
    "Input - Canon - Curve - Canon-Log",
    "Input - Canon - Curve - Canon-Log2",
    "Input - Canon - Curve - Canon-Log3",
    "Input - Canon - Linear - Canon Rec. 709 Daylight",
    "Input - Canon - Linear - Canon Rec. 709 Tungsten",
    "Input - Canon - Linear - Canon DCI-P3 Daylight",
    "Input - Canon - Linear - Canon DCI-P3 Tungsten",
    "Input - Canon - Linear - Canon Cinema Gamut Daylight",
    "Input - Canon - Linear - Canon Cinema Gamut Tungsten",
    "Input - Canon - Linear - Canon Rec. 2020 Daylight",
    "Input - Canon - Linear - Canon Rec. 2020 Tungsten",
    "Input - Generic - sRGB - Texture",
    "Input - GoPro - Protune Flat - Protune Native - Experimental",
    "Input - GoPro - Curve - Protune Flat",
    "Input - GoPro - Linear - Protune Native - Experimental",
    "Input - Panasonic - V-Log - V-Gamut",
    "Input - Panasonic - Curve - V-Log",
    "Input - Panasonic - Linear - V-Gamut",
    "Input - RED - REDlogFilm - DRAGONcolor",
    "Input - RED - REDlogFilm - DRAGONcolor2",
    "Input - RED - REDlogFilm - REDcolor",
    "Input - RED - REDlogFilm - REDcolor2",
    "Input - RED - REDlogFilm - REDcolor3",
    "Input - RED - REDlogFilm - REDcolor4",
    "Input - RED - REDLog3G10 - REDWideGamutRGB",
    "Input - RED - Curve - REDlogFilm",
    "Input - RED - Curve - REDLog3G10",
    "Input - RED - Linear - DRAGONcolor",
    "Input - RED - Linear - DRAGONcolor2",
    "Input - RED - Linear - REDcolor",
    "Input - RED - Linear - REDcolor2",
    "Input - RED - Linear - REDcolor3",
    "Input - RED - Linear - REDcolor4",
    "Input - RED - Linear - REDWideGamutRGB",
    "Input - Sony - S-Log1 - S-Gamut",
    "Input - Sony - S-Log2 - S-Gamut",
    "Input - Sony - S-Log2 - S-Gamut Daylight",
    "Input - Sony - S-Log2 - S-Gamut Tungsten",
    "Input - Sony - S-Log3 - S-Gamut3.Cine",
    "Input - Sony - S-Log3 - S-Gamut3",
    "Input - Sony - Curve - S-Log1",
    "Input - Sony - Curve - S-Log2",
    "Input - Sony - Curve - S-Log3",
    "Input - Sony - Linear - S-Gamut",
    "Input - Sony - Linear - S-Gamut Daylight",
    "Input - Sony - Linear - S-Gamut Tungsten",
    "Input - Sony - Linear - S-Gamut3.Cine",
    "Input - Sony - Linear - S-Gamut3",
    "Output - DCDM (P3 gamut clip) -- output only",
    "Output - DCDM",
    "Output - P3-D60 ST2084 (1000 nits) -- output only",
    "Output - P3-D60 ST2084 (2000 nits) -- output only",
    "Output - P3-D60 ST2084 (4000 nits) -- output only",
    "Output - Rec.2020 ST2084 (1000 nits) -- output only",
    "Output - P3-D60",
    "Output - P3-DCI",
    "Output - Rec.2020",
    "Output - Rec.709",
    "Output - Rec.709 (D60 sim.)",
    "Output - sRGB",
    "Output - sRGB (D60 sim.)",
    "Utility - LMT Shaper",
    "Utility - Log2 48 nits Shaper",
    "Utility - Log2 48 nits Shaper - AP1",
    "Utility - Log2 1000 nits Shaper",
    "Utility - Log2 1000 nits Shaper - AP1",
    "Utility - Log2 2000 nits Shaper",
    "Utility - Log2 2000 nits Shaper - AP1",
    "Utility - Log2 4000 nits Shaper",
    "Utility - Log2 4000 nits Shaper - AP1",
    "Utility - Dolby PQ 10000",
    "Utility - Dolby PQ 48 nits Shaper",
    "Utility - Dolby PQ 48 nits Shaper - AP1",
    "Utility - Dolby PQ 1000 nits Shaper",
    "Utility - Dolby PQ 1000 nits Shaper - AP1",
    "Utility - Dolby PQ 2000 nits Shaper",
    "Utility - Dolby PQ 2000 nits Shaper - AP1",
    "Utility - Dolby PQ 4000 nits Shaper",
    "Utility - Dolby PQ 4000 nits Shaper - AP1",
    "Utility - Curve - Rec.1886",
    "Utility - Curve - Rec.2020",
    "Utility - Curve - Rec.709",
    "Utility - Curve - sRGB",
    "Utility - Linear - Adobe RGB",
    "Utility - Linear - Adobe Wide Gamut RGB",
    "Utility - Linear - P3-D60",
    "Utility - Linear - P3-D65",
    "Utility - Linear - P3-DCI",
    "Utility - Linear - RIMM ROMM (ProPhoto)",
    "Utility - Linear - Rec.2020",
    "Utility - Linear - Rec.709",
    "Utility - Linear - sRGB",
    "Utility - Rec.2020 - Camera",
    "Utility - Rec.2020 - Display",
    "Utility - Rec.709 - Camera",
    "Utility - Rec.709 - Display",
    "Utility - XYZ - D60",
    "Utility - sRGB - Texture",
    "Utility - Raw",
    "Utility - Look - ACES 1.0 to 0.1 emulation -- output only",
    "Utility - Look - ACES 1.0 to 0.2 emulation -- output only",
    "Utility - Look - ACES 1.0 to 0.7 emulation -- output only"
)

SOURCE_COLORS = COLOR_SPACES
OUTPUT_COLORS = COLOR_SPACES

RESIZE_CHOICES = { 'original', '4K', '1440p', '1080p', '720p', 'custom' }
RESIZE_PARSES = {
    "original":"original",
    "4K":"3840:2160",
    "1440p":"2560:1440",
    "1080p":"1920:1080",
    "720p":"1280:720"
}

NAME_MODE_CHOICES = { 'shot_cut_folder', 'folder', 'first_frame', 'comp_v###' }

ROOT_FOLDER = tk.StringVar(value=os.getcwd())

OPT_SINGLE_INPUT_FOLDER = tk.StringVar(value="")
OPT_SINGLE_OUTPUT_FILE = tk.StringVar(value="")
OPT_SINGLE_SKIP_OCIO = tk.BooleanVar(value=True)

OPT_VIDEO_FORMAT = tk.StringVar(value=VIDEO_FORMATS[1])
OPT_SOURCE_COLOR = tk.StringVar(value=SOURCE_COLORS[0])
OPT_OUTPUT_COLOR = tk.StringVar(value=OUTPUT_COLORS[0])
OPT_NUM_THREADS = tk.IntVar(value=16)
OPT_DELETE_TEMP = tk.BooleanVar(value=True)
OPT_OVERWRITE = tk.BooleanVar(value=False)
OPT_CRF = tk.StringVar(value="15")
OPT_ASPECT = tk.StringVar(value="original")
OPT_FPS = tk.StringVar(value="25")
OPT_DRAW_TEXT = tk.BooleanVar(value=True)
OPT_RESIZE_CHOICE = tk.StringVar(value="original")
OPT_CUSTOM_RESIZE = tk.StringVar(value="1920:1080")
OPT_NAME_MODE = tk.StringVar( value='shot_cut_folder' )
OPT_ADD_DST_COLOR = tk.BooleanVar(value=False)
OPT_PRORES = tk.BooleanVar(value=False)
OPT_KEEP_JPG = tk.BooleanVar(value=False)

PRESET_FILETYPE = [("exr ocio preset","*.preset") ]

def on_close_window():
    print("closing ui")
    opt_str = options_to_string()
    f = open("options.dat", "w")
    f.write(opt_str)
    try:
        root.destroy()
    except:
        pass

root.protocol("WM_DELETE_WINDOW", on_close_window)

def get_resize_value():
    if OPT_RESIZE_CHOICE.get() == "custom":
        return OPT_CUSTOM_RESIZE.get()
    elif OPT_RESIZE_CHOICE.get() == "original":
        return "original"
    else:
        return RESIZE_PARSES[OPT_RESIZE_CHOICE.get()]

def start_process():
    c = 'python "'+os.getcwd()+'/main.py"'
    c += ' --dir "'+str(ROOT_FOLDER.get())+'"'
    c += ' --format "'+str(OPT_VIDEO_FORMAT.get())+'"'
    c += ' --src-space "'+str(OPT_SOURCE_COLOR.get())+'"'
    c += ' --dst-space "'+str(OPT_OUTPUT_COLOR.get())+'"'
    c += ' --threads '+str(OPT_NUM_THREADS.get())
    c += ' --overwrite '+str(OPT_OVERWRITE.get())
    c += ' --crf "'+str(OPT_CRF.get())+'"'
    c += ' --aspect "'+str(OPT_ASPECT.get())+'"'
    c += ' --fps '+str(OPT_FPS.get())
    c += ' --draw-text ' + str(OPT_DRAW_TEXT.get())
    c += ' --resize ' + get_resize_value()
    c += ' --name-mode '+str(OPT_NAME_MODE.get())
    c += ' --append-dst '+str(OPT_ADD_DST_COLOR.get())
    c += ' --prores '+str(OPT_PRORES.get())
    c += ' --keep-jpg '+str(OPT_KEEP_JPG.get())
    print(c)
    os.system(c)

opt_custom_resize_label = None
opt_custom_resize = None

def dir_click():
    ROOT_FOLDER.set(filedialog.askdirectory())
    print(ROOT_FOLDER.get())

# need to refactor option storing and handling into JSON or something....
def options_to_string():
    s = ""
    s += OPT_VIDEO_FORMAT.get()
    s += "\n"+str(OPT_SOURCE_COLOR.get())
    s += "\n"+str(OPT_OUTPUT_COLOR.get())
    s += "\n"+str(OPT_NUM_THREADS.get())
    s += "\n"+str(OPT_DELETE_TEMP.get())
    s += "\n"+str(OPT_OVERWRITE.get())
    s += "\n"+str(OPT_CRF.get())
    s += "\n"+str(ROOT_FOLDER.get())
    s += "\n"+str(OPT_ASPECT.get())
    s += "\n"+str(OPT_FPS.get())
    s += "\n"+str(OPT_DRAW_TEXT.get())
    s += "\n"+str(OPT_SINGLE_INPUT_FOLDER.get())
    s += "\n"+str(OPT_SINGLE_OUTPUT_FILE.get())
    s += "\n"+str(OPT_SINGLE_SKIP_OCIO.get())
    s += "\n"+str(OPT_RESIZE_CHOICE.get())
    s += "\n"+str(OPT_CUSTOM_RESIZE.get())
    s += "\n"+str(OPT_NAME_MODE.get())
    s += "\n"+str(OPT_ADD_DST_COLOR.get())
    s += "\n"+str(OPT_PRORES.get())
    s += "\n"+str(OPT_KEEP_JPG.get())
    return s

def set_options_from_string( opt_string ):
    lines = opt_string.split("\n")
    OPT_VIDEO_FORMAT.set( lines[0] )
    OPT_SOURCE_COLOR.set( lines[1] )
    OPT_OUTPUT_COLOR.set( lines[2] )
    OPT_NUM_THREADS.set( lines[3] )
    OPT_DELETE_TEMP.set( lines[4] )
    OPT_OVERWRITE.set( lines[5] )
    OPT_CRF.set( lines[6] )
    ROOT_FOLDER.set( lines[7] )
    OPT_ASPECT.set( lines[8] )
    OPT_FPS.set(lines[9])
    OPT_DRAW_TEXT.set(lines[10])
    OPT_SINGLE_INPUT_FOLDER.set(lines[11])
    OPT_SINGLE_OUTPUT_FILE.set(lines[12])
    OPT_SINGLE_SKIP_OCIO.set(lines[13])
    OPT_RESIZE_CHOICE.set(lines[14])
    OPT_CUSTOM_RESIZE.set(lines[15])
    OPT_NAME_MODE.set(lines[16])
    OPT_ADD_DST_COLOR.set(lines[17])
    OPT_PRORES.set(lines[18])
    OPT_KEEP_JPG.set(lines[19])

def save_preset():
    s = options_to_string()
    f = filedialog.asksaveasfile( filetypes=PRESET_FILETYPE, defaultextension=PRESET_FILETYPE )
    if f == None:
        print("Cancelled saving preset!")
        return
    f.write( s )
    f.close()
    print("preset saved : "+f.name)
    preset_info["text"] = f.name

def load_preset():
    f = filedialog.askopenfile( mode='r', filetypes=PRESET_FILETYPE )
    if f == None:
        print("No preset file specified!")
        return
    s = f.read()
    set_options_from_string( s )
    print("loaded preset from file : "+f.name)
    preset_info["text"] = f.name

try:
    opt_file = open("options.dat", "r")
    set_options_from_string( opt_file.read() )
except:
    pass

############################
# TKINTER UI WIDGETS START #
############################

preset_frame = tk.LabelFrame( root, text="Preset" )
preset_frame.pack( fill=tk.X, anchor=tk.N, ipadx=4, ipady=4, padx=4, pady=4 )

preset_load_btn = tk.Button( preset_frame, text=" Load ", command=load_preset )
preset_load_btn.pack( side=tk.LEFT )

preset_save_btn = tk.Button( preset_frame, text=" Save ", command=save_preset )
preset_save_btn.pack( side=tk.LEFT )

preset_info = tk.Label( preset_frame, text="current preset : None" )
preset_info.pack( side=tk.LEFT )

dir_frame = tk.LabelFrame( root, text="Sequence Root Folder", height=32)
dir_frame.pack( fill=tk.X, anchor=tk.N, ipadx=4, ipady=4, padx=4, pady=4 )

dir_btn = tk.Button(dir_frame, text="...", command=dir_click, height = 1)
dir_btn.pack( side=tk.LEFT, anchor=tk.NW)

dir_text = tk.Entry( dir_frame, textvariable=ROOT_FOLDER )
dir_text.pack( side=tk.LEFT, anchor=tk.NW, expand=1, fill=tk.X )

opt_frame = tk.LabelFrame( root, text="Options (advanced!)")
opt_frame.pack( fill=tk.BOTH, expand=1, anchor=tk.NW, ipadx=4, ipady=4, padx=4, pady=4 )

tk.Grid.columnconfigure( opt_frame, 1, weight=1 )

row = 0
opt_name_mode_label = tk.Label(opt_frame, text="Output Name Mode")
opt_name_mode_label.grid(row=row, column=0, sticky=tk.W)
opt_name_mode = tk.OptionMenu(opt_frame, OPT_NAME_MODE, *NAME_MODE_CHOICES)
opt_name_mode.grid(row=row, column=1, sticky=tk.E)

row += 1
opt_keep_jpg_label = tk.Label(opt_frame, text="Keep JPG Sequence")
opt_keep_jpg_label.grid( row=row, column=0, sticky=tk.W )
opt_keep_jpg = tk.Checkbutton( opt_frame, variable=OPT_KEEP_JPG )
opt_keep_jpg.grid( row=row, column=1, sticky=tk.E)

row += 1
opt_add_dst_color_label = tk.Label(opt_frame,text="Append Output Colorspace to Name")
opt_add_dst_color_label.grid(row=row, column=0, sticky=tk.W)
opt_add_dst_color = tk.Checkbutton( opt_frame, variable=OPT_ADD_DST_COLOR )
opt_add_dst_color.grid( row=row, column=1, sticky=tk.E )
row += 1
opt_source_color_label = tk.Label(opt_frame, text="Source Color Space")
opt_source_color_label.grid(row=row, column=0, sticky=tk.W)
opt_source_color = ttk.Combobox(opt_frame, textvariable=OPT_SOURCE_COLOR, values=SOURCE_COLORS)
opt_source_color.grid(row=row, column=1, sticky=tk.EW)
row += 1
opt_output_color_label = tk.Label(opt_frame, text="Output Color Space")
opt_output_color_label.grid(row=row, column=0, sticky=tk.W)
opt_output_color = ttk.Combobox(opt_frame, textvariable=OPT_OUTPUT_COLOR, values=OUTPUT_COLORS)
opt_output_color.grid(row=row, column=1, sticky=tk.EW)
row += 1
opt_num_threads_label = tk.Label(opt_frame, text="OCIO threads")
opt_num_threads_label.grid(row=row, column=0, sticky=tk.W)
opt_num_threads = ttk.Entry(opt_frame, textvariable=OPT_NUM_THREADS)
opt_num_threads.grid(row=row, column=1, sticky=tk.E)
row += 1
#opt_output_file_format_label = tk.Label(opt_frame, text="Output File Format")
#opt_output_file_format_label.grid(row=row, column=0, sticky=tk.W)
#opt_output_file_format = ttk.Combobox(opt_frame, textvariable=OPT_VIDEO_FORMAT, values=VIDEO_FORMATS)
#opt_output_file_format.grid(row=row, column=1, sticky=tk.E)
#row += 1

opt_prores_label = tk.Label(opt_frame, text="encode prores")
opt_prores_label.grid(row=row, column=0, sticky=tk.W)
opt_prores = tk.Checkbutton( opt_frame, variable=OPT_PRORES )
opt_prores.grid(row=row, column=1, sticky=tk.E)
row += 1

opt_overwrite_label = tk.Label(opt_frame, text="Process and overwrite Existing")
opt_overwrite_label.grid(row=row, column=0, sticky=tk.W)
opt_overwrite = ttk.Checkbutton(opt_frame, variable=OPT_OVERWRITE)
opt_overwrite.grid(row=row, column=1, sticky=tk.E)
row += 1
#opt_delete_temp_label = tk.Label(opt_frame, text="Delete Temp files")
#opt_delete_temp_label.grid(row=4, column=0, sticky=tk.W)
#opt_delete_temp = ttk.Checkbutton(opt_frame, variable=OPT_DELETE_TEMP)
#opt_delete_temp.grid(row=4, column=1, sticky=tk.E)

opt_crf_label = tk.Label(opt_frame, text="CRF (0-51) (0:lossless, 51:worst)")
opt_crf_label.grid(row=row, column=0, sticky=tk.W)
opt_crf = ttk.Entry(opt_frame, textvariable=OPT_CRF)
opt_crf.grid(row=row, column=1, sticky=tk.E)
row += 1
opt_aspect_label = tk.Label(opt_frame, text="Aspect ('original' or 'w:h' example: '2.39:1')")
opt_aspect_label.grid( row=row, column=0, sticky=tk.W )
opt_aspect = ttk.Entry( opt_frame, textvariable=OPT_ASPECT )
opt_aspect.grid( row=row, column=1, sticky=tk.E )
row += 1
opt_fps_label = tk.Label( opt_frame, text="FPS" )
opt_fps_label.grid( row=row, column=0, sticky=tk.W )
opt_fps = ttk.Entry( opt_frame, textvariable=OPT_FPS )
opt_fps.grid( row=row, column=1, sticky=tk.E )
row += 1
opt_draw_text_label = tk.Label(opt_frame, text="Draw text")
opt_draw_text_label.grid(row=row, column=0, sticky=tk.W)
opt_draw_text = ttk.Checkbutton(opt_frame, variable=OPT_DRAW_TEXT)
opt_draw_text.grid(row=row, column=1, sticky=tk.E)
row += 1
opt_resize_choice_label = tk.Label( opt_frame, text="resize" )
opt_resize_choice = tk.OptionMenu( opt_frame, OPT_RESIZE_CHOICE, *RESIZE_CHOICES )
opt_resize_choice.grid( row=row, column=1, sticky=tk.E )
opt_resize_choice_label.grid( row=row, column=0, sticky=tk.W )
row += 1
opt_custom_resize_label = tk.Label( opt_frame, text="custom resize (ffmpeg scale syntax 'w:h' example: '1280:720')")
opt_custom_resize = tk.Entry( opt_frame, textvariable=OPT_CUSTOM_RESIZE )
custom_resize_row = row
def on_resize_change(*args):
    show_custom = OPT_RESIZE_CHOICE.get() == "custom"
    if show_custom:
        opt_custom_resize.grid( row=custom_resize_row, column=1, sticky=tk.E)
        opt_custom_resize_label.grid(row=custom_resize_row, column=0, sticky=tk.W)
    else:
        opt_custom_resize_label.grid_forget()
        opt_custom_resize.grid_forget()
row += 1
on_resize_change()
OPT_RESIZE_CHOICE.trace('w', on_resize_change)

execute_btn = tk.Button(root, text="Start Process !", command=start_process)
execute_btn.pack( side=tk.TOP, anchor=tk.N, expand=1, fill=tk.X)

# FOOTER
logo = ImageTk.PhotoImage(Image.open(FROSTFX_LOGO))
logo_obj = tk.Label(root, image=logo)
logo_obj.pack(side=tk.LEFT, expand=1, anchor=tk.SW)

author = tk.Label(root, text="kalev@frostfx.ee")
author.pack(side=tk.RIGHT, anchor=tk.SE)

root.mainloop()