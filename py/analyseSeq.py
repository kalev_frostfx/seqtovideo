from filesearch import search_for_exr_files
import OpenEXR
import tkinter as tk
from tkinter import filedialog
from tkinter import ttk
from PIL import ImageTk, Image
import os

# tk UI
root = tk.Tk()
root.iconbitmap( default='frostfx_logo.ico' )
root.geometry("500x200")
root.title("EXR Attribute Analysis")

ROOT_FOLDER = tk.StringVar(value=os.getcwd())

def start_process():
    FOLDERS = [x[0] for x in os.walk(ROOT_FOLDER.get())]
    marks = ""
    for folder in FOLDERS:
        search_res = search_for_exr_files( folder )
        if len( search_res["exr_list"] ) != 0:
            mark = False
            marks += folder +"," + search_res["shot_name"]+os.sep+search_res["ver_name"]
            minx = 0
            miny = 0
            maxx = 0
            maxy = 0
        
            for exr in search_res["exr_list"]:
                exr_file = OpenEXR.InputFile( folder+os.sep+exr )
                minx = exr_file.header()["dataWindow"].min.x
                miny = exr_file.header()["dataWindow"].min.y
                maxx = exr_file.header()["dataWindow"].max.x
                miny = exr_file.header()["dataWindow"].max.y
                if exr_file.header()["displayWindow"] != exr_file.header()["dataWindow"]:
                    minx = exr_file.header()["dataWindow"].min.x
                    miny = exr_file.header()["dataWindow"].min.y
                    maxx = exr_file.header()["dataWindow"].max.x
                    miny = exr_file.header()["dataWindow"].max.y
                    mark = True
                    break
            marks += ","+str(minx)
            marks += ","+str(miny)
            marks += ","+str(maxx)
            marks += ","+str(maxy)
            if mark:
                print( "marked " + search_res["shot_name"] + " ver " + search_res["ver_name"] )
                marks += ",0\n"
            else:
                marks += ",1\n"
    f = open(os.getcwd() +os.sep+ "analysis_result.txt", "w")
    f.write(marks)
    f.close()
    print("WROTE results into : "+os.getcwd() +os.sep+ "analysis_result.txt")

def dir_click():
    ROOT_FOLDER.set(filedialog.askdirectory())
    print(ROOT_FOLDER.get())

dir_frame = tk.LabelFrame( root, text="Sequence Root Folder", height=32)
dir_frame.pack( fill=tk.X, anchor=tk.N, ipadx=4, ipady=4, padx=4, pady=4 )

dir_btn = tk.Button(dir_frame, text="...", command=dir_click, height = 1)
dir_btn.pack( side=tk.LEFT, anchor=tk.NW)

dir_text = tk.Entry( dir_frame, textvariable=ROOT_FOLDER )
dir_text.pack( side=tk.LEFT, anchor=tk.NW, expand=1, fill=tk.X )

execute_btn = tk.Button(root, text="Analyse !", command=start_process)
execute_btn.pack( side=tk.TOP, anchor=tk.N, expand=1, fill=tk.X)

logo = ImageTk.PhotoImage(Image.open("frostfx_logo.png"))
logo_obj = tk.Label(root, image=logo)
logo_obj.pack(side=tk.LEFT, expand=1, anchor=tk.SW)

author = tk.Label(root, text="made by: Kalev Mölder @ FrostFX")
author.pack(side=tk.RIGHT, anchor=tk.SE)

root.mainloop()