import os
from os import walk
import time
import multiprocessing
import sys
import getopt
import subprocess
from filesearch import search_for_image_files
import fractions
import re
from environment import BIN_PYTHON, BIN_OIIOTOOL, BIN_OCIO_IINFO, BIN_FFMPEG, OCIO_CONFIG, FONT
import uuid
import shutil
from typing import List

SESSION_ID = str(uuid.uuid1())
SOURCE_PATHS_TXT = os.path.abspath( "../temp/src_paths_%s.txt" % (SESSION_ID) )
OUTPUT_JPG_PATHS_TXT = os.path.abspath( "../temp/output_jpg_paths_%s.txt" % (SESSION_ID) )
OUTPUT_EXR_PATHS_TXT = os.path.abspath( "../temp/output_exr_paths_%s.txt" % (SESSION_ID) )
SEQUENCE_PATHS_JPG_TXT = os.path.abspath( "../temp/sequence_paths_jpg_%s.txt" % (SESSION_ID) )
SEQUENCE_PATHS_EXR_TXT = os.path.abspath( "../temp/sequence_paths_exr_%s.txt" % (SESSION_ID) )

CRED = '\033[91m'
CGREEN = '\033[42m'
CEND = '\033[0m'

ROOT_FOLDER = ""
FOLDERS:List[str] = []
OPT_VIDEO_FORMAT = 'mp4'
OPT_SRC_COLOR = 'aces'
OPT_DST_COLOR = 'out_srgb'
OPT_THREADS = "16"
OPT_OVERWRITE = False
OPT_CRF = "25"
OPT_FPS = "25"
OPT_ASPECT = "original"
OPT_DRAW_TEXT = True
OPT_RESIZE = "original"
OPT_NAME_MODE = "shot_cut_folder"
OPT_APPEND_DST_COLOR = False
OUT = os.getcwd()+"/out.mp4"
OPT_PRORES = False
OPT_KEEP_JPG = False

out:str = ""

original_aspect = 1
f_w = 0
f_h = 0
a_x = 0
a_y = 0
a = 1
crop_w = 0
crop_h = 0
crop_x = 0
crop_y = 0
pad_w = 0
pad_h = 0
pad_x = 0
pad_y = 0
decimal_aspect = 0
fractional_aspect = 0

bar_height = 0

os.environ["OCIO"] = OCIO_CONFIG

paths_per_thread = 10
startTime = 0

decimal_aspect = 0
fractional_aspect = 0

def divide_chunks(l, n):  
    for i in range(0, len(l), n):  
        yield l[i:i + n] 

def process_folder( folder ):
    global a_x
    global a_y
    global decimal_aspect
    global fractional_aspect
    print("Starting Process")
    #print(dir(OpenEXR))
    search_res = search_for_image_files( folder )

    if len(search_res["image_list"]) == 0:
        return
    first_frame_path = os.path.join(folder, search_res["image_list"][0])
    iinfo_string = str(subprocess.check_output([ BIN_OCIO_IINFO, first_frame_path ]))
    rematch = re.search( r':\s+(\d+)\s+x\s+(\d+)', iinfo_string )
    if rematch == None:
        print("COULD NOT GET IMAGE INFO")
        return
    print(rematch.group(1), rematch.group(2))
    file_w = int(rematch.group(1))
    file_h = int(rematch.group(2))
    original_aspect = file_w / file_h
    # checking aspect
    if not OPT_ASPECT == 'original':
        if not ':' in OPT_ASPECT:
            print("Not ':' in aspect option. aborting...")
            return
        a_x = float(OPT_ASPECT.split(':')[0])
        a_y = float(OPT_ASPECT.split(':')[1])
        a = a_y / a_x
        new_height = file_w * a
        height_diff = file_h - new_height
        half_height_diff = height_diff / 2
        bar_height = int(half_height_diff)
    
    # constructing output file path
    if OPT_NAME_MODE == "shot_cut_folder":
        out = search_res["shot_dir"]+"_"+search_res["ver_name"]
    elif OPT_NAME_MODE == "folder":
        out = os.path.join(folder, "../", search_res["ver_name"])
    elif OPT_NAME_MODE == "first_frame":
        framename = search_res["image_list"][0]
        #framename.pop( len(framename)-1 )
        #framename = ''.join(framename)
        framenum = re.search(r'(\d+)(\..*)', framename)
        framename = framename.replace( framenum.group(1), '' )
        framename = framename.replace( framenum.group(2), '' )
        out = os.path.join(folder, "../", framename)
    elif OPT_NAME_MODE == "comp_v###":
        framename = search_res["image_list"][0]
        #framename.pop( len(framename)-1 )
        #framename = ''.join(framename)
        print("FRAMENAME", framename)
        framename = re.sub( r'(.+)(v\d\d\d.+)', r'\1comp_\2', framename )
        print("FRAMENAME SUB", framename)
        framenum = re.search(r'(\d+)(\..*)', framename)
        print("FRAMENUM", framenum)
        framename = framename.replace( framenum.group(1), '' )
        framename = framename.replace( framenum.group(2), '' )
        out = os.path.join(folder, "../", framename)
    if OPT_APPEND_DST_COLOR:
        out += "_"+OPT_DST_COLOR
    
    out = out.replace(os.sep, '/')
    out = out.rstrip('_')

    out_exr = out + ".mov"
    out_jpg = out + ".mp4"

    
    if os.path.exists( out_exr ):
        if not OPT_OVERWRITE:
            print(out_exr +" EXISTS SKIPPING...")
            return
    if os.path.exists( out_jpg ):
        if not OPT_OVERWRITE:
            print(out_jpg +" EXISTS SKIPPING...")
            return

    out_exr = '"'+out_exr+'"'
    out_jpg = '"'+out_jpg+'"'
 
    src_dat = open(SOURCE_PATHS_TXT, 'w')
    for frame in search_res["image_list"]:
        src_dat.write( os.path.normpath(folder+'/'+frame)+'\n')
    src_dat.close()

    seqpaths_jpg = open(SEQUENCE_PATHS_JPG_TXT,"w")
    seqpaths_exr = open(SEQUENCE_PATHS_EXR_TXT,"w")
    out_jpg_dat = open(OUTPUT_JPG_PATHS_TXT, 'w')
    out_exr_dat = open(OUTPUT_EXR_PATHS_TXT, 'w')

    jpg_folder = os.path.normpath( os.path.join( folder, os.path.basename(folder)+'_OCIO_JPG' ) )
    exr_folder = os.path.normpath( os.path.join( folder, os.path.basename(folder)+'_OCIO_EXR' ) )
    
    if not os.path.exists(jpg_folder):
        os.mkdir(jpg_folder)
    if not os.path.exists(exr_folder):
        os.mkdir(exr_folder)

    for frame in search_res["image_list"]:
        p_jpg = os.path.normpath(jpg_folder+'/'+frame+'.jpg')
        p_jpg = os.path.abspath(p_jpg)

        p_exr = os.path.normpath(exr_folder+'/'+frame+'.exr')
        p_exr = os.path.abspath(p_exr)

        out_jpg_dat.write( p_jpg +'\n' )
        out_exr_dat.write( p_exr +'\n' )
        if OPT_PRORES:
            seqpaths_exr.write( "file '"+p_exr+"'\n" )
        
        seqpaths_jpg.write( "file '"+p_jpg+"'\n")

    out_jpg_dat.close()
    out_exr_dat.close()
    seqpaths_exr.close()
    seqpaths_jpg.close()

    # CONVERT TO JPG

    c = BIN_PYTHON+' ocio_convert.py --src-space "%s" --dst-space "%s" --threads %s --src-paths "%s" --dst-paths "%s"' % ( 
        OPT_SRC_COLOR, 
        OPT_DST_COLOR, 
        OPT_THREADS,
        SOURCE_PATHS_TXT,
        OUTPUT_JPG_PATHS_TXT
    )

    print("OCIOCONVERT CMD >>", c)
    os.system(c)
    print("\n")

    # CONVERT TO EXR

    if OPT_PRORES:
        c = BIN_PYTHON+' ocio_convert.py --src-space "%s" --dst-space "%s" --threads %s --src-paths "%s" --dst-paths "%s"' % ( 
            OPT_SRC_COLOR, 
            OPT_DST_COLOR, 
            OPT_THREADS,
            SOURCE_PATHS_TXT,
            OUTPUT_EXR_PATHS_TXT
        )
    
        print("OCIOCONVERT CMD >>", c)
        os.system(c)
        print("\n")

    name_exr = out_exr.split('/')
    name_exr = name_exr[len(name_exr)-1]
    name_exr = name_exr.replace('"', '')

    name_jpg = out_jpg.split('/')
    name_jpg = name_jpg[len(name_jpg)-1]
    name_jpg = name_jpg.replace('"', '')

    # FFMPEG COMMAND

    # MOV PRORES
    if OPT_PRORES:
        ffmpeg_cmd = '%s -y -f concat -safe 0 -r %s -i "%s"' % ( BIN_FFMPEG, OPT_FPS, os.path.abspath(SEQUENCE_PATHS_EXR_TXT)) 
        ffmpeg_cmd += " -crf "+OPT_CRF

        ffmpeg_cmd += ' -c:v prores_ks -profile:v 1'
        ffmpeg_cmd += ' -vf "format=yuv422p10le'
        if OPT_RESIZE != "original":
            ffmpeg_cmd += ' ,scale=%s' % (OPT_RESIZE)
        if OPT_ASPECT != "original":
            ffmpeg_cmd += ' ,drawbox=x=%s:y=%s:w=%s:h=%s:t=fill' % ( 0,0,"in_w",bar_height )
            ffmpeg_cmd += ' ,drawbox=x=%s:y=in_h-%s:w=%s:h=%s:t=fill' % ( 0,bar_height,"in_w",bar_height )
        if OPT_DRAW_TEXT:
            ffmpeg_cmd += " ,drawtext=text=%s <> %s:fontfile=%s:fontsize=64:fontcolor=white:bordercolor=black:borderw=4:x=(w-text_w)/2:y=0" % ( name_exr, OPT_ASPECT.replace(":","/"), FONT)
        ffmpeg_cmd += '"'
        ffmpeg_cmd += ' -r %s' % (OPT_FPS)
        ffmpeg_cmd += " "+out_exr
        print("FFMPEG_CMD")
        subprocess.call(ffmpeg_cmd)
    
    # MP4
    ffmpeg_cmd = '%s -y -f concat -safe 0 -r %s -i "%s"' % ( BIN_FFMPEG, OPT_FPS, os.path.abspath(SEQUENCE_PATHS_JPG_TXT)) 
    ffmpeg_cmd += " -crf "+OPT_CRF

    ffmpeg_cmd += ' -vf "format=pix_fmts=yuv420p'
    if OPT_RESIZE != "original":
        ffmpeg_cmd += ' ,scale=%s' % (OPT_RESIZE)
    if OPT_ASPECT != "original":
        ffmpeg_cmd += ' ,drawbox=x=%s:y=%s:w=%s:h=%s:t=fill' % ( 0,0,"in_w",bar_height )
        ffmpeg_cmd += ' ,drawbox=x=%s:y=in_h-%s:w=%s:h=%s:t=fill' % ( 0,bar_height,"in_w",bar_height )
    if OPT_DRAW_TEXT:
        ffmpeg_cmd += " ,drawtext=text=%s <> %s:fontfile=%s:fontsize=64:fontcolor=white:bordercolor=black:borderw=4:x=(w-text_w)/2:y=0" % ( name_jpg, OPT_ASPECT.replace(":","/"), FONT)
    ffmpeg_cmd += '"'
    ffmpeg_cmd += ' -r %s' % (OPT_FPS)
    ffmpeg_cmd += " "+out_jpg
    print("FFMPEG_CMD")
    subprocess.call(ffmpeg_cmd)

    print("Deleting temp files")
    if os.path.exists(jpg_folder) and not OPT_KEEP_JPG:
        shutil.rmtree( jpg_folder )
    if os.path.exists(exr_folder):
        shutil.rmtree( exr_folder )
    os.remove(SOURCE_PATHS_TXT)
    os.remove(SEQUENCE_PATHS_JPG_TXT)
    os.remove(SEQUENCE_PATHS_EXR_TXT)
    print("Temp files deleted!")

def process_root_folder():
    startTime = time.perf_counter()
    FOLDERS = [x[0] for x in os.walk(ROOT_FOLDER)]
    for folder in FOLDERS:
        fol = folder.replace('/', os.sep)
        process_folder(fol)
    print( "VIDEO GENERATION COMPLETED" )
    print("Ended process with "+str(time.perf_counter()-startTime)+" ms")

if __name__ == '__main__':
    try:
        opts, args = getopt.getopt(
            sys.argv[1:],
            "",
            [
                "dir=",
                "format=",
                "src-space=",
                "dst-space=",
                "threads=",
                "overwrite=",
                "crf=",
                "aspect=",
                "fps=",
                "draw-text=",
                "resize=",
                "name-mode=",
                "append-dst=",
                "prores=",
                "keep-jpg="
            ]
        )
    except getopt.GetoptError:
        print("invalid arguments. Following arguments must be present:")
        print("--dir (root folder which contains sequences)")
        print("--format (video output format. mp4 or mov)")
        print("--src-space (source ocio color space. aces, acescg, etc)")
        print("--dst-space (destination ocio color space. srgb)")
        print("--threads (number of threads to use when applying ocio)")
        print("--overwrite (wether to process and overwrite if video exists)")
        print("--crf (output video quality (0-51) passed to ffmpeg)")
        print("--aspect (aspect which to letterbox the video into. frame size remains the same)")
        print("--fps (output video framerate)")
        print("--draw-text (wether to overlay draw a text with sequence name)")
        print("--resize (wether to resize output)")
        print("--name-mode")
        print("--append-dst")
        print("--prores")
        sys.exit()
    for opt, arg in opts:
        print("option : "+opt+"     argument : "+arg)
        if opt == '--dir':
            ROOT_FOLDER = arg
        elif opt == '--format':
            OPT_VIDEO_FORMAT = arg
        elif opt == '--src-space':
            OPT_SRC_COLOR = arg
        elif opt == '--dst-space':
            OPT_DST_COLOR = arg
        elif opt == '--threads':
            OPT_THREADS = arg
        elif opt == '--overwrite':
            OPT_OVERWRITE = (arg == "True")
        elif opt == '--crf':
            OPT_CRF = arg
        elif opt == '--aspect':
            OPT_ASPECT = arg
        elif opt == '--fps':
            OPT_FPS = arg
        elif opt == '--draw-text':
            OPT_DRAW_TEXT = (arg == "True")
        elif opt == '--resize':
            OPT_RESIZE = arg
        elif opt == '--name-mode':
            OPT_NAME_MODE = arg
        elif opt == '--append-dst':
            OPT_APPEND_DST_COLOR = (arg == "True")
        elif opt == '--prores':
            OPT_PRORES = (arg == "True")
        elif opt == '--keep-jpg':
            OPT_KEEP_JPG = (arg == "True")
    process_root_folder()