import os
from os import walk

IMAGE_FORMATS = [ ".exr", ".dpx" ]

def search_for_image_files( folder ):
    if "OCIO_JPG" in folder or "OCIO_EXR" in folder:
        print("Skipping folder "+folder+ "path contains OCIO_JPG or OCIO_EXR.")
        return {
            "image_list" : [],
            "shot_dir" : "",
            "shot_name" : "",
            "ver_name" : ""
        }
    print("search", folder)
    #EXR = []
    IMAGES = []
    f = []
    for (dirpath, dirnames, filenames) in walk(folder):
        f.extend(filenames)
        break
    for path in f:
        name, ext = os.path.splitext(path)
        if ext in IMAGE_FORMATS:
            IMAGES.append(path)

    if len(IMAGES) == 0:
        return {
            "image_list" : [],
            "shot_dir" : "",
            "shot_name" : "",
            "ver_name" : ""
        }

    IMAGES.sort()

    ver_name = os.path.basename(folder)
    shot_name = folder.split(os.sep)
    del shot_name[-1]
    shot_dir = os.sep.join(shot_name)
    shot_name = os.path.basename(shot_dir)
    shot_dir += os.sep+shot_name
    
    return { 
        "image_list" : IMAGES,
        "shot_dir" : shot_dir,
        "shot_name" : shot_name,
        "ver_name" : ver_name
    }