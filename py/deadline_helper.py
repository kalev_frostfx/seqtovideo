#!/usr/bin/python

import os
import sys
import getopt
from environment import BIN_PYTHON

# commandline to be executed from deadline to convert a job result to video after rendering

# quick and dirty solution for now...
# going to be refactored when option handling gets overhaul

OPT_VIDEO_FORMAT = ""
OPT_SOURCE_COLOR = ""
OPT_OUTPUT_COLOR = ""
OPT_NUM_THREADS = ""
OPT_DELETE_TEMP = ""
OPT_OVERWRITE = ""
OPT_CRF = ""
ROOT_FOLDER = ""
OPT_ASPECT = ""
OPT_FPS = ""
OPT_DRAW_TEXT = ""
OPT_SINGLE_INPUT_FOLDER = ""
OPT_SINGLE_OUTPUT_FILE = ""
OPT_SINGLE_SKIP_OCIO = ""
OPT_RESIZE_CHOICE = ""
OPT_CUSTOM_RESIZE = ""
OPT_NAME_MODE = ""
OPT_ADD_DST_COLOR = ""
OPT_PRORES = "False"
OPT_KEEP_JPG = "False"

RESIZE_PARSES = {
    "original":"original",
    "4K":"3840:2160",
    "1440p":"2560:1440",
    "1080p":"1920:1080",
    "720p":"1280:720"
}

def get_resize_value():
    if OPT_RESIZE_CHOICE == "custom":
        return OPT_CUSTOM_RESIZE
    elif OPT_RESIZE_CHOICE in {"original", ""}:
        return "original"
    else:
        return RESIZE_PARSES[OPT_RESIZE_CHOICE]

def main(argv):
    folder = ''
    preset = ''

    try:
        opts, args = getopt.getopt(argv, "hf:p:", ["folder=", "preset="])
    except getopt.GetoptError:
        sys.exit(2)
    
    for opt, arg, in opts:
        if opt == '-h':
            print('-f <seq folder path> -p <preset file path>')
            sys.exit()
        elif opt in ("-f", "--folder"):
            folder = arg
        elif opt in ("-p", "--preset"):
            preset = arg
    
    preset_file = open( preset, 'r' )

    # terrible I know...
    lines = preset_file.read().split('\n')
    OPT_VIDEO_FORMAT = lines[0]
    OPT_SOURCE_COLOR = lines[1]
    OPT_OUTPUT_COLOR = lines[2]
    OPT_NUM_THREADS = lines[3]
    OPT_DELETE_TEMP = lines[4]
    OPT_OVERWRITE = lines[5]
    OPT_CRF = lines[6]
    ROOT_FOLDER = folder
    OPT_ASPECT = lines[8]
    OPT_FPS = lines[9]
    OPT_DRAW_TEXT = lines[10]
    OPT_SINGLE_INPUT_FOLDER = lines[11]
    OPT_SINGLE_OUTPUT_FILE = lines[12]
    OPT_SINGLE_SKIP_OCIO = lines[13]
    OPT_RESIZE_CHOICE = lines[14]
    OPT_CUSTOM_RESIZE = lines[15]
    OPT_NAME_MODE = lines[16]
    OPT_ADD_DST_COLOR = lines[17]
    OPT_PRORES = lines[18]
    OPT_KEEP_JPG = lines[19]

    c = BIN_PYTHON+' main.py'
    c += ' --dir "%s"' % (ROOT_FOLDER)
    c += ' --format "%s"' % (OPT_VIDEO_FORMAT)
    c += ' --src-space "%s"' % (OPT_SOURCE_COLOR)
    c += ' --dst-space "%s"' % (OPT_OUTPUT_COLOR)
    c += ' --threads "%s"' % (OPT_NUM_THREADS)
    c += ' --overwrite "%s"' % (OPT_OVERWRITE)
    c += ' --crf "%s"' % (OPT_CRF)
    c += ' --aspect "%s"' % (OPT_ASPECT)
    c += ' --fps "%s"' % (OPT_FPS)
    c += ' --draw-text "%s"' % (OPT_DRAW_TEXT)
    c += ' --resize "%s"' % (get_resize_value())
    c += ' --name-mode "%s"' % (OPT_NAME_MODE)
    c += ' --append-dst "%s"' % (OPT_ADD_DST_COLOR)
    c += ' --prores "%s"' % (OPT_PRORES)
    c += ' --keep-jpg "%s"' % (OPT_KEEP_JPG)
    print(c)
    os.system(c)

if __name__ == "__main__":
    main(sys.argv[1:])