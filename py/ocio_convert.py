import sys
import multiprocessing
import os
import getopt
import subprocess
from environment import BIN_OIIOTOOL

OPT_THREADS = 16
OPT_SRC_COLOR = 's_log3_s_gamut3Cine'
OPT_DST_COLOR = 'out_srgb'
OPT_SOURCES = []
OPT_SRC_PATHS = ""
OPT_DST_APPEND = ".jpg"
OPT_OUTPUTS = []

def threaded_ocio( source, src_color, dst_color, output  ):
    #return sources
    #for source in sources:
        #print source
    #print("OCIO_PATH "+OCIO_path)
    #command = BIN_OCIO_CONVERT+' "'+source+'" "'+src_color+'" "'+source+'.jpg" "'+dst_color+'"'
    command = BIN_OIIOTOOL + ' "%s" --colorconvert "%s" "%s" -o "%s"' % ( source, src_color, dst_color, output )
    #os.system(command)
    subprocess.call(command, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    print(".",end="")
    sys.stdout.flush()

if __name__ == '__main__':

    try:
        opts, args = getopt.getopt(
            sys.argv[1:],
            "",
            [
                "src-space=",
                "dst-space=",
                "threads=",
                "src-paths=",
                "dst-paths="
            ]
        )
    except getopt.GetoptError:
        print("invalid arguments. Following arguments must be present:")
        print("--src-space (source ocio color space. aces, acescg, etc)")
        print("--dst-space (destination ocio color space. srgb)")
        print("--threads (number of threads to use when applying ocio)")
        print("--src-paths (path to file containing list of source images to be converted)")
        print("--dst-paths (path to file containing list of output paths for the converted files)")
        sys.exit()
    for opt, arg in opts:
        print("option : "+opt+"     argument : "+arg)
        if opt == '--src-space':
            OPT_SRC_COLOR = arg
        elif opt == '--dst-space':
            OPT_DST_COLOR = arg
        elif opt == '--threads':
            OPT_THREADS = int(arg)
        elif opt == '--src-paths':
            f = open(arg, 'r')
            OPT_SRC_PATHS = f.read()
            f.close()
        elif opt == '--dst-paths':
            f = open(arg, 'r')
            OPT_DST_PATHS = f.read()
            f.close()

    for line in OPT_SRC_PATHS.split('\n'):
        OPT_SOURCES.append(line)

    src_color_list = (OPT_SRC_COLOR,)*len(OPT_SOURCES)
    dst_color_list = (OPT_DST_COLOR,)*len(OPT_SOURCES)
    dst_append_list = (OPT_DST_APPEND,)*len(OPT_SOURCES)
    for line in OPT_DST_PATHS.split('\n'):
        OPT_OUTPUTS.append(line)

    #print("THREADS "+str(OPT_THREADS))
    pool = multiprocessing.Pool(processes=OPT_THREADS)
    #pool.map(threaded_ocio, OPT_SOURCES)
    pool.starmap( threaded_ocio, zip( OPT_SOURCES, src_color_list, dst_color_list, OPT_OUTPUTS ) )
