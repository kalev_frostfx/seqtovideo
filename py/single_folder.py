# copied from main.py
# a quick hack to support single folder processing

import os
from os import walk
import time
import multiprocessing
import sys
import getopt
import subprocess
from filesearch import search_for_exr_files
import OpenEXR
import fractions

CRED = '\033[91m'
CGREEN = '\033[42m'
CEND = '\033[0m'

ROOT_FOLDER = ""
FOLDERS = []
OIIO = os.path.join( os.getcwd(),"OpenImageIO-1.6.18-bin-vc9-x64" )
OCIO_CONFIG = os.path.join( os.getcwd(),"OcioConfig/aces_1.0.3/config.ocio" )
OPT_VIDEO_FORMAT = 'mp4'
OPT_SRC_COLOR = 'aces'
OPT_DST_COLOR = 'out_srgb'
OPT_THREADS = 16
OPT_OVERWRITE = False
OPT_CRF = "25"
OPT_FPS = 25
OPT_ASPECT = "original"
OPT_DRAW_TEXT = True
OPT_OUTPUT = os.getcwd()+"/out.mp4"
OPT_SKIP_OCIO = False

FONT_PATH = os.path.join( os.getcwd(), "Inconsolata-Regular.ttf" )
FONT_PATH = FONT_PATH.replace(os.sep, '/')

original_aspect = 1
f_w = 0
f_h = 0
a_x = 0
a_y = 0
a = 1
crop_w = 0
crop_h = 0
crop_x = 0
crop_y = 0
pad_w = 0
pad_h = 0
pad_x = 0
pad_y = 0
decimal_aspect = 0
fractional_aspect = 0

bar_height = 0

os.environ["OCIO"] = OCIO_CONFIG

paths_per_thread = 10
threads = []
startTime = 0

decimal_aspect = 0
fractional_aspect = 0

def divide_chunks(l, n):  
    for i in range(0, len(l), n):  
        yield l[i:i + n] 

def process_folder( folder ):
    global a_x
    global a_y
    global decimal_aspect
    global fractional_aspect
    print("Starting Process")
    print(dir(OpenEXR))
    search_res = search_for_exr_files( folder )

    if len(search_res["exr_list"]) == 0:
        return
    first_frame_path = os.path.join(folder, search_res["exr_list"][0])
    exr_file = OpenEXR.InputFile( first_frame_path )
    file_w = exr_file.header()["displayWindow"].max.x - exr_file.header()["displayWindow"].min.x
    file_h = exr_file.header()["displayWindow"].max.y - exr_file.header()["displayWindow"].min.y
    print( exr_file.header() )
    original_aspect = file_w / file_h
    # checking aspect
    if not OPT_ASPECT == 'original': 
        if not ':' in OPT_ASPECT:
            print("Not ':' in aspect option. aborting...")
            return
        a_x = float(OPT_ASPECT.split(':')[0])
        a_y = float(OPT_ASPECT.split(':')[1])
        a = a_y / a_x
        new_height = file_w * a
        height_diff = file_h - new_height
        half_height_diff = height_diff / 2
        bar_height = int(half_height_diff)

    #out = search_res["shot_dir"]+"_"+search_res["ver_name"]+"_"+OPT_DST_COLOR+"."+OPT_VIDEO_FORMAT
    #out = out.replace(os.sep, '/')
    #print("OUTPUT FILENAME "+out)
    
    out = OPT_OUTPUT

    if os.path.exists( out ):
        if not OPT_OVERWRITE:
            print(out +" EXISTS SKIPPING...")
            return

    out = '"'+out+'"'

    src_dat = open('sourcepaths.dat', 'w')
    sourcepaths = []
    for frame in search_res["exr_list"]:
        sourcepaths.append(folder+'/'+frame)
        src_dat.write( folder+'/'+frame+'\n')
    
    if OPT_SKIP_OCIO:
        seqpaths = open("seqpaths.txt","w")
        for frame in search_res["exr_list"]:
            framepath = os.path.normpath(folder+"/"+frame)
            framepath = framepath.replace(os.sep, '/')
            seqpaths.write("file '"+framepath+"'\n")
        seqpaths.close()
    else:
        c = 'python "'+os.getcwd()+'/ocio_convert.py"'
        c += ' --src-space "'+OPT_SRC_COLOR+'"'
        c += ' --dst-space "'+OPT_DST_COLOR+'"'
        c += ' --threads '+str(OPT_THREADS)
        src_dat.close()
        os.system(c)
        print("\n")
        
        tempFiles = []
        seqpaths = open("seqpaths.txt","w")
        for frame in search_res["exr_list"]:
            framepath = os.path.normpath(folder+"/"+frame)+".jpg"
            framepath = framepath.replace(os.sep, '/')
            tempFiles.append(framepath)
            seqpaths.write("file '"+framepath+"'\n")
        seqpaths.close()

    sequenceFile = "seqpaths.txt"
    name = out.split('/')
    name = name[len(name)-1]
    name = name.replace('"', '')

    # FFMPEG COMMAND
    ffmpeg_cmd = os.getcwd()+"/ffmpeg -y -f concat -safe 0 -r "+str(OPT_FPS)+" -i "+sequenceFile
    ffmpeg_cmd += " -probesize 100M"
    ffmpeg_cmd += " -analyzeduration 100M"
    ffmpeg_cmd += " -c:v libx264"
    ffmpeg_cmd += " -crf "+OPT_CRF
    ffmpeg_cmd += ' -r %s -vf "format=pix_fmts=yuv420p' % (OPT_FPS)
    if OPT_ASPECT != "original":
        ffmpeg_cmd += ' ,drawbox=x=%s:y=%s:w=%s:h=%s:t=fill' % ( 0,0,"in_w",bar_height )
        ffmpeg_cmd += ' ,drawbox=x=%s:y=in_h-%s:w=%s:h=%s:t=fill' % ( 0,bar_height,"in_w",bar_height )
    if OPT_DRAW_TEXT:
        ffmpeg_cmd += " ,drawtext=text=%s <> %s:fontfile=%s:fontsize=64:fontcolor=white:bordercolor=black:borderw=4:x=(w-text_w)/2:y=0" % ( name, OPT_ASPECT.replace(":","/"), FONT_PATH)
    ffmpeg_cmd += '"'
    ffmpeg_cmd += " "+out
    print("FFMPEG_CMD", ffmpeg_cmd + " --enable-libfreetype --enable-libfontconfig" )
    subprocess.call(ffmpeg_cmd)

    if not OPT_SKIP_OCIO:
        print("Deleting temp files")
        for tempFile in tempFiles:
            os.remove( tempFile )
            print(".",end="")
            sys.stdout.flush()
        print("\n")
        print("Temp files deleted!")

def process_root_folder():
    startTime = time.perf_counter()
    folder = ROOT_FOLDER
    fol = folder.replace('/', os.sep)
    process_folder(fol)
    print( "MP4 GENERATION COMPLETED" )
    print("Ended process with "+str(time.perf_counter()-startTime)+" ms")

if __name__ == '__main__':
    try:
        opts, args = getopt.getopt(
            sys.argv[1:],
            "",
            [
                "dir=",
                "format=",
                "src-space=",
                "dst-space=",
                "threads=",
                "overwrite=",
                "crf=",
                "aspect=",
                "fps=",
                "draw-text=",
                "skip_ocio=",
                "out="
            ]
        )
    except getopt.GetoptError:
        print("invalid arguments")
        sys.exit()
    for opt, arg in opts:
        print("option : "+opt+"     argument : "+arg)
        if opt == '--dir':
            ROOT_FOLDER = arg
        elif opt == '--format':
            OPT_VIDEO_FORMAT = arg
        elif opt == '--src-space':
            OPT_SRC_COLOR = arg
        elif opt == '--dst-space':
            OPT_DST_COLOR = arg
        elif opt == '--threads':
            OPT_THREADS = arg
        elif opt == '--overwrite':
            OPT_OVERWRITE = (arg == "true")
        elif opt == '--crf':
            OPT_CRF = arg
        elif opt == '--aspect':
            OPT_ASPECT = arg
        elif opt == '--fps':
            OPT_FPS = arg
        elif opt == '--draw-text':
            OPT_DRAW_TEXT = (arg == "True")
        elif opt == '--skip_ocio':
            OPT_SKIP_OCIO = (arg == "True")
        elif opt == '--out':
            OPT_OUTPUT = arg
    process_root_folder()