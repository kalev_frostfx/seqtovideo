# SeqToVideo #

This is a python program to search and render image sequences 
into videos while also applying OCIO colorspace conversion.

### Supported Image Sequences ###
*	EXR
*	DPX

### Installation ###

Just copy the root folder anywhere. All dependencies are bundled.
Works only on 64bit windows!

### Contact ###
kalev@frostfx.ee