this is a simple plugin for Deadline to launch SeqToVideo after a job has finished rendering.

usage:
	copy OCIOVideo folder to your deadline repository events folder.
	there are some settings that need to be modified as a super user in Deadline such as
	setting a path to SeqToVideo installation.

	when submitting a new job add the comment "seqtovideo:PRESET_NAME_HERE".
	the preset that is to be used must be present in SeqToVideo folder.
	
	a new commandline job is created procedurally to convert the rendered image sequence to video.