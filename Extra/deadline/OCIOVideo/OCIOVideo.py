from Deadline.Events import *
from Deadline.Scripting import *

import os

def GetDeadlineEventListener():
    return OCIOVideo()

def CleanupDeadlineEventListener( deadlinePlugin ):
    deadlinePlugin.Cleanup()

class OCIOVideo( DeadlineEventListener ):
    def __init__(self):
        self.OnJobFinishedCallback += self.OnJobFinished
    
    def Cleanup(self):
        del self.OnJobFinishedCallback

    def OnJobFinished(self, job):
        self.LogInfo("[ER] - OCIO Video Start")
        if not "seqtovideo" in job.JobComment:
            self.LogInfo("[ER] - No seqtovideo in comments. doing nothing...")
            return
        
        if not "debug" in job.JobComment:
            if len(job.JobOutputDirectories) == 0:
                self.LogInfo("[ER] - No OutputDirectories in job info. aborting...")
                return

        # needs a regex in the future, will fail if more comments are around!
        preset_file_name = job.JobComment.split( ':' )[1]
        if not ".preset" in preset_file_name:
            preset_file_name += ".preset"
        preset_file_path = os.path.normpath( os.path.join( self.GetConfigEntry("SeqToVideoPath"), preset_file_name ) )

        job_file_path = os.path.normpath(
            os.path.join(
                self.GetConfigEntry( "TempJobFilePath" ),
                job.JobName+".txt"
            )
        )
        SEQTOVIDEO = self.GetConfigEntry("SeqToVideoPath")

        folder = ""

        if "debug" in job.JobComment:
            folder = "seq_folder_here"
        else:
            folder =  os.path.normpath(job.JobOutputDirectories[0])

        # setting current work dir
        cmd = '%s && cd "%s" &&' % ( SEQTOVIDEO[0:2], os.path.normpath( SEQTOVIDEO))
        cmd += ' launch_from_deadline.bat "%s" "%s"' % (folder, preset_file_path )
        #cmd += ' -f "%s"' % ( os.path.normpath(job.JobOutputDirectories[0]) )
        #cmd += ' -p "%s"' % ( preset_file_path )

        self.LogInfo("[ER] - job_file_path = "+job_file_path)
        job_file = open( job_file_path, 'w')
        job_file.write("Plugin=CommandLine\n")
        
        job_file.write("Name=%s\n" % ( "seqtovideo__"+ job.JobName))
        
        job_file.write("Executable=\n")
        job_file.write("ShellExecute=True\n")
        job_file.write("Arguments=%s\n" % (cmd))
        job_file.write("Shell=cmd\n")
        job_file.write("MachineLimit=1\n")
        job_file.write("Priority=99\n")
        job_file.write("Whitelist=%s\n" % (self.GetConfigEntry("Machine")))
        job_file.close()

        job_file_path = job_file_path.replace( '\\', '/' )
        self.LogInfo("[ER] - job_file_path replaced sep = "+job_file_path)
        RepositoryUtils.SubmitJob([ job_file_path, job_file_path, job_file_path ])
        self.LogInfo("[ER] - OCIOVideo finished")